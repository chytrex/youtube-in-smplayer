#!/usr/bin/ruby1.8

if ARGV[0].nil? or ARGV[0].empty? then
	at_exit { puts "No link as parameter."}
	exit
else
	link = "#{ARGV[0]}"
end

if link =~ /http[s]?:\/\/www\.youtube\.com\/watch.+/i or link =~ /http[s]?:\/\/youtu\.be\/.+/i
	exec "smplayer \"#{link}\""
elsif link =~ /http[s]?:\/\/i\.imgur\.com\/(.+)\.webm/i or link =~ /http[s]?:\/\/i\.imgur\.com\/(.+)\.gifv/i or link =~ /http[s]?:\/\/i\.imgur\.com\/(.+)\.mp4/i
	link = "http://i.imgur.com/#{$1}.mp4"
	exec "smplayer -no-close-at-end -actions \"repeat true\" \"#{link}\""
elsif link =~ /http[s]?:\/\/i\.4cdn\.org\/.+\/.+\.webm/i
	link.sub!(/https/, "http")
	exec "smplayer -no-close-at-end -actions \"repeat false\" \"#{link}\""
elsif link =~ /http[s]?:\/\/zippy\.gfycat\.com\/(.+)\.webm/i
	link = "http://zippy.gfycat.com/#{$1}.webm"
	exec "smplayer -no-close-at-end -actions \"repeat false\" \"#{link}\""
elsif link =~ /http:\/\/.*\.apcdn\.com\/.+\.webm/i
	exec "smplayer -no-close-at-end -actions \"repeat false\"  \"#{link}\""
else
	exec "x-www-browser \"#{link}\""
end
